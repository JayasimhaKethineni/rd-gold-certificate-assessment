package com.epam.project.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.project.dto.AssociateDto;
import com.epam.project.dto.AssociateUpdateDto;
import com.epam.project.exception.AssociateException;
import com.epam.project.model.Associate;
import com.epam.project.repository.AssociateRepository;
import com.epam.project.repository.BatchRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AssociateService {

	@Autowired
	private AssociateRepository associateRepository;
	
	@Autowired
	private BatchRepository batchRepository;
	
	
	@Autowired
	private ModelMapper mapper;
	
	public AssociateDto addAssociate(AssociateDto associateDto) {
		log.info("invoked AddAssociate method... with associate as "+associateDto);
		Associate associate=mapper.map(associateDto, Associate.class);
		batchRepository.save(associate.getBatch());
		associateRepository.save(associate);
		return associateDto;
	}
	
	public List<AssociateDto> getAssociatesByGender(String gender) {
		log.info("invoked AddAssociate method... with gender as "+gender);
		List<Associate> associates=associateRepository.findAllByGender(gender);
		return associates.stream().map(associate->mapper.map(associate, AssociateDto.class)).toList();
	}
	public AssociateUpdateDto updateAssociate(AssociateUpdateDto associateUpdateDto) {
		log.info("invoked AddAssociate method... with new associate and old id as "+associateUpdateDto);
		Associate associate=mapper.map(associateUpdateDto, Associate.class);
		associateRepository.save(associate);
		return mapper.map(associate, AssociateUpdateDto.class);
	}
	public void deleteAssociate(int id) {
		log.info("invoked AddAssociate method... with id: "+id);
		Associate associate=associateRepository.findById(id).orElseThrow(()->new AssociateException("Associate Not Found With Id : "+id));
		associateRepository.delete(associate);
	}
}
