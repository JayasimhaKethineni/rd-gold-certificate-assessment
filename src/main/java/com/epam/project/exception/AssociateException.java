package com.epam.project.exception;

public class AssociateException extends RuntimeException{

	
	public AssociateException(String message) {
		super(message);
	}
	
}
