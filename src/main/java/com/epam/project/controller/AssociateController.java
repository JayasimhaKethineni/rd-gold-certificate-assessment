package com.epam.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.project.dto.AssociateDto;
import com.epam.project.dto.AssociateUpdateDto;
import com.epam.project.service.AssociateService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/associates")
@Slf4j
public class AssociateController {
	@Autowired
	private AssociateService associateService;
	
	@PostMapping
	public ResponseEntity<AssociateDto> addAssociate(@RequestBody @Valid AssociateDto associateDto){
		log.info("addAssociate controller is invoked... with associate as "+associateDto);
		return new ResponseEntity<>(associateService.addAssociate(associateDto), HttpStatus.CREATED);
	}
	
	@GetMapping("/{gender}")
	public ResponseEntity<List<AssociateDto>> getAssociatesByGender(@PathVariable String gender){
		log.info("getAssociatesByGender controller is invoked... with gender as "+gender);
		return new ResponseEntity<>(associateService.getAssociatesByGender(gender), HttpStatus.OK);
	}
	@PutMapping
	public ResponseEntity<AssociateUpdateDto> updateAssociate(@RequestBody @Valid AssociateUpdateDto associateUpdateDto){
		log.info("updateAssociate controller is invoked... with new associate and old id as "+associateUpdateDto);
		return new ResponseEntity<>(associateService.updateAssociate(associateUpdateDto), HttpStatus.ACCEPTED);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteAssociate(@PathVariable int id){
		log.info("deleteAssociate controller is invoked...with id: "+id);
		associateService.deleteAssociate(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);

	}
}
