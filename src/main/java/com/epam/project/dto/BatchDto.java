package com.epam.project.dto;

import java.util.Date;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class BatchDto {
	private int id;
	@NotBlank(message = "Don't leave name field empty!!!")
	private String name;
	@NotBlank(message = "Don't leave practice field empty!!!")
	private String practice;
	private Date startDate;
	private Date endDate;
	
}


