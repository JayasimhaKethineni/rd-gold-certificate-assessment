package com.epam.project.dto;

import com.epam.project.model.Batch;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AssociateUpdateDto {
	@Min(value = 1)
	private int oldId;
	@NotBlank(message = "Don't leave name field empty!!!")
	private String name;
	@NotBlank(message = "Don't leave email field empty!!!")
	@Email(message = "please provide valid email !!!")
	private String email;
	@NotBlank(message = "Don't leave gender field empty!!!")
	@Pattern(regexp = "MALE|FEMALE",message = "please provide either of them(MALE,FEMALE)")
	private String gender;
	@NotBlank(message = "Don't leave college field empty!!!")
	private String college;
	@NotBlank(message = "Don't leave status field empty!!!")
	@Pattern(regexp = "ACTIVE|INACTIVE",message = "please provide either of them(ACTIVE,INACTIVE)")
	private String status;
	private BatchDto batchDto;
}
