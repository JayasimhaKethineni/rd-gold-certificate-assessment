package com.epam.project.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.project.model.Associate;


public interface AssociateRepository extends JpaRepository<Associate, Integer>  {
	List<Associate> findAllByGender(String gender);
}
