package com.epam.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.project.model.Batch;

public interface BatchRepository extends JpaRepository<Batch, Integer>{

}
