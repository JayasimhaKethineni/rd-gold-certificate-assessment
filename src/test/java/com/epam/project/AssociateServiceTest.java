package com.epam.project;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.project.dto.AssociateDto;
import com.epam.project.dto.AssociateUpdateDto;
import com.epam.project.dto.BatchDto;
import com.epam.project.exception.AssociateException;
import com.epam.project.model.Associate;
import com.epam.project.model.Batch;
import com.epam.project.repository.AssociateRepository;
import com.epam.project.repository.BatchRepository;
import com.epam.project.service.AssociateService;

@ExtendWith(MockitoExtension.class)
public class AssociateServiceTest {

	@Mock
	private AssociateRepository associateRepository;
	
	@Mock
	private ModelMapper mapper;
	
	@Mock
	private BatchRepository batchRepository;
	
	@InjectMocks
	private AssociateService associateService;
	
	@BeforeEach
	public void setUp() {
		Batch batch=new Batch(1000,"RDIN","Java",new Date(),new Date());
		Associate associate = new Associate(1000,"new","new@gmail.com","MALE","college","status",batch);
		BatchDto batchDto=new BatchDto(1000,"RDIN","Java",new Date(),new Date());
		AssociateDto associateDto = new AssociateDto(1000,"new","new@gmail.com","MALE","college","status",batchDto);
		
	}
	
	@Test
	void addAssociateTest() {
		Batch batch=new Batch(1000,"RDIN","Java",new Date(),new Date());
		Associate associate = new Associate(1000,"new","new@gmail.com","MALE","college","status",batch);
		BatchDto batchDto=new BatchDto(1000,"RDIN","Java",new Date(),new Date());
		AssociateDto associateDto = new AssociateDto(1000,"new","new@gmail.com","MALE","college","status",batchDto);
		
		Mockito.when(mapper.map(associateDto, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepository.save(associate.getBatch())).thenReturn(batch);
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		associateService.addAssociate(associateDto);
		Mockito.verify(mapper).map(associateDto, Associate.class);
		Mockito.verify(batchRepository).save(associate.getBatch());
		Mockito.verify(associateRepository).save(associate);

	} 
	@Test 
	void updateAssociateTest() {
		Batch batch=new Batch(1000,"RDIN","Java",new Date(),new Date());
		Associate associate = new Associate(1000,"new","new@gmail.com","MALE","college","status",batch);
		BatchDto batchDto=new BatchDto(1000,"RDIN","Java",new Date(),new Date());
		
		AssociateUpdateDto associateUpdateDto=new AssociateUpdateDto(1000,"new","new@gmail.com","MALE","college","status",batchDto);
		
		Mockito.when(mapper.map(associateUpdateDto, Associate.class)).thenReturn(associate);
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		Mockito.when(mapper.map(associate, AssociateUpdateDto.class)).thenReturn(associateUpdateDto);

		associateService.updateAssociate(associateUpdateDto);
		Mockito.verify(mapper).map(associateUpdateDto, Associate.class);
		Mockito.verify(associateRepository).save(associate);
		Mockito.verify(mapper).map(associate, AssociateUpdateDto.class);
	} 
	@Test
	void getAssociatesByGenderTest() {
		Batch batch=new Batch(1000,"RDIN","Java",new Date(),new Date());
		Associate associate = new Associate(1000,"new","new@gmail.com","MALE","college","status",batch);
		BatchDto batchDto=new BatchDto(1000,"RDIN","Java",new Date(),new Date());
		AssociateDto associateDto = new AssociateDto(1000,"new","new@gmail.com","MALE","college","status",batchDto);

		
		List<Associate> list=new ArrayList<>();
		list.add(associate);
		String gender ="MALE";
		Mockito.when(associateRepository.findAllByGender(gender)).thenReturn(list);
		Mockito.when(mapper.map(associate, AssociateDto.class)).thenReturn(associateDto);

		associateService.getAssociatesByGender(gender);
		Mockito.verify(associateRepository).findAllByGender(gender);
		Mockito.verify(mapper).map(associate, AssociateDto.class);

	} 
	@Test
	void deleteAssociateTest() {
		Batch batch=new Batch(1000,"RDIN","Java",new Date(),new Date());
		Associate associate = new Associate(1000,"new","new@gmail.com","MALE","college","status",batch);
		
		Mockito.when(associateRepository.findById(1000)).thenReturn(Optional.of(associate));
		Mockito.doNothing().when(associateRepository).delete(associate);
		associateService.deleteAssociate(1000);
		Mockito.verify(associateRepository).findById(1000);
		Mockito.verify(associateRepository).delete(associate);

	} 
	@Test
	void deleteAssociateTestException() {
		Batch batch=new Batch(1000,"RDIN","Java",new Date(),new Date());
		Associate associate = new Associate(1000,"new","new@gmail.com","MALE","college","status",batch);
		
		Mockito.when(associateRepository.findById(1000)).thenReturn(Optional.empty());
		
		assertThrows(AssociateException.class, ()->associateService.deleteAssociate(1000));
		Mockito.verify(associateRepository).findById(1000);

	} 
	
	
	
}
