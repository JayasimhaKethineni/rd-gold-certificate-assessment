package com.epam.project;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.project.controller.AssociateController;
import com.epam.project.dto.AssociateDto;
import com.epam.project.dto.AssociateUpdateDto;
import com.epam.project.dto.BatchDto;
import com.epam.project.model.Associate;
import com.epam.project.model.Batch;
import com.epam.project.service.AssociateService;
import com.fasterxml.jackson.databind.ObjectMapper;


@WebMvcTest(AssociateController.class)
public class AssociateControllerTest {

	@Autowired 
	private MockMvc mockMvc;
	
	@MockBean 
	private AssociateService associateService;
	

	@Test
	void addAssociateTest() throws Exception{
		Batch batch=new Batch(1000,"RDIN","Java",new Date(),new Date());
		Associate associate = new Associate(1000,"new","new@gmail.com","MALE","college","status",batch);
		BatchDto batchDto=new BatchDto(1000,"RDIN","Java",new Date(),new Date());
		AssociateDto associateDto = new AssociateDto(1000,"new","new@gmail.com","MALE","college","ACTIVE",batchDto);
		mockMvc.perform(post("/associates").contentType(MediaType.APPLICATION_JSON).content(asJsonString(associateDto))).andExpect(status().isCreated());
	}
	@Test
	void getAssociatesByGenderTest() throws Exception{
//		Batch batch=new Batch(1000,"RDIN","Java",new Date(),new Date());
//		Associate associate = new Associate(1000,"new","new@gmail.com","MALE","college","status",batch);
//		BatchDto batchDto=new BatchDto(1000,"RDIN","Java",new Date(),new Date());
//		AssociateDto associateDto = new AssociateDto(1000,"new","new@gmail.com","MALE","college","ACTIVE",batchDto);
		mockMvc.perform(get("/associates/MALE")).andExpect(status().isOk());
	}
	@Test
	void deleteByIdTest() throws Exception{
		mockMvc.perform(delete("/associates/1")).andExpect(status().isNoContent());
	}
	@Test
	void updateAssociate() throws Exception{
		BatchDto batchDto=new BatchDto(1000,"RDIN","Java",new Date(),new Date());
		AssociateUpdateDto associateUpdateDto=new AssociateUpdateDto(1000,"new","new@gmail.com","MALE","college","ACTIVE",batchDto);
		mockMvc.perform(put("/associates").contentType(MediaType.APPLICATION_JSON).content(asJsonString(associateUpdateDto))).andExpect(status().isAccepted());
	}
	
	
	private static String asJsonString(Object object) throws Exception{
		ObjectMapper objectMapper =new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
}
